import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas
// document.querySelector('nav> a').innerHTML += `<small>les pizzas c'est la vie</small>`
const disp = document.querySelector('section');
disp.setAttribute('style', '');

aboutPage = new Component('section', null, 'Ce site est génial');
pizzaForm = new Component(
	'section',
	null,
	'Ici vous pourrez ajouter une pizza'
);

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];
Router.menuElement = document.querySelector('.mainMenu');
